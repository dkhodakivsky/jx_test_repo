import hudson.model.Result
import jenkins.model.Jenkins

@NonCPS
def checkStatusJob(job_name) {
    def job = Jenkins.instance.getItem("${job_name}")
    // Check if last five builds of job are successful
    if (job.builds[0..4].find { it.result != Result.SUCCESS }) {
        return "Last five builds weren't successful. Waiting for really true fixes"
    } else {
        return "Last five builds are successful and ready for promotion!"
    }

}

def jxParseYamlFile(yaml_file) {

}

// Generate random name for deploying on k8s
def ganeratePhrase(predefined_string = 'pre-stage-'){
    return predefined_string + shellUtils.pipe("cat /dev/urandom | tr -dc 'a-z' | fold -w 7 | head -n 1").trim()
}

// Get latest branch that was merged in master
def latestMergedBranch () {
    return shellUtils.pipe("git show :/^Merge | grep Merged | awk '{print \$3}'")
}

// Get staging env for current setup. Clone repo. Change permissions
def getStagingRepo(STAGING_REPO_DIR) {
     shellUtils.pipe("jx get env | grep ${STAGING_REPO_DIR} | awk '{print \$7}' | xargs -I {} git clone {} ${STAGING_REPO_DIR}")
     shellUtils.pipe("chown 10000:10000 ${STAGING_REPO_DIR} -R")
}
