def notifySlack(buildStatus, additionalMessage = null) {
    // Build status of null means success.
    buildStatus = buildStatus ?: 'SUCCESS'

    def color

    if (buildStatus == 'SUCCESS') {
        color = 'good'
    } else if (buildStatus == 'NOT_BUILT') {
        color = '#6F747C'
    } else if (buildStatus == 'UNSTABLE') {
        color = '#FFFE89'
    } else {
        color = 'danger'
    }

    def message = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
    if (additionalMessage != null) {
        message += "\n" + additionalMessage
    }

    sendToSlack([color: color, message: message])
}

def publishRtp(messageText) {
    rtp parserName: 'Confluence',
        nullAction: '1',
        stableText: messageText
}

// temporarily to testing. rewrite channel to variable
def sendToSlack(data) {
    slackSend channel: "${env.SLACK_CHANNEL}",
              color: data.color,
              message: data.message
}
