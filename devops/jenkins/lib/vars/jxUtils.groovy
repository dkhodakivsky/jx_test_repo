import hudson.model.Result
import jenkins.model.Jenkins

@NonCPS
def checkStatusJob(job_name) {
    def job = Jenkins.instance.getItem("${job_name}")
    // Check if last five builds of job are successful
    if (job.builds[0..4].find { it.result != Result.SUCCESS }) {
        return "Last five builds weren't successful. Waiting for really true fixes"
    } else {
        return "Last five builds are successful and ready for promotion!"
    }

}

def jxParseYamlFile(yaml_file) {

}

// Generate random name for deploying on k8s
def ganeratePhrase(predefined_string = 'pre-stage-'){
    return predefined_string + shellUtils.pipe("cat /dev/urandom | tr -dc 'a-z' | fold -w 7 | head -n 1").trim()
}

// Get latest branch that was merged in master
def latestMergedBranch () {
    return shellUtils.pipe("git show :/^Merge | grep Merged | awk '{print \$3}'")
}

// Get staging env for current setup. Clone repo. Change permissions
def getStagingRepo(STAGING_REPO_DIR) {
     shellUtils.pipe("jx get env | grep ${STAGING_REPO_DIR} | awk '{print \$7}' | xargs -I {} git clone {} ${STAGING_REPO_DIR}")
}

// Changed owner. 10000 - jenkins
def changeOwner (folder) {
    shellUtils.pipe("chown 10000:10000 ${folder} -R")
}

// Delete Namespace
def DeleteNamespace(K8S_NAMESPACE) {
    shellUtils.pipe("kubectl delete ns ${K8S_NAMESPACE} || true")
    shellUtils.pipe("kubectl delete configmap ${K8S_NAMESPACE}.v1 -n kube-system || true")
}

// Create Namespace
def CreateNamespace(K8S_NAMESPACE) {
    shellUtils.pipe("kubectl create ns ${K8S_NAMESPACE}")
}

// Install fonts for Headless Chrome
def InstallDeps() {
    shellUtils.pipe("yum install ipa-gothic-fonts xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi xorg-x11-utils xorg-x11-fonts-cyrillic xorg-x11-fonts-Type1 xorg-x11-fonts-misc -y")
}

// Check if all pods are READY
def CheckRediness(TEST_NAMESPACE) {
   shellUtils.pipe("""
                  for i in `seq 1 10`;
                  do
                      ### Get list of pods in json format, and get boolean variable of READY state for all pods.
                      ready_status=\$(kubectl get pods -n \${TEST_NAMESPACE} -o jsonpath='{.items[*].status.containerStatuses[0].ready}')

                      ### Check if \$ready_status not empty and include bolean values: false|False|FALSE.
                      if [[ ! -z \$ready_status && \$ready_status =~ .*false|False|FALSE.* ]]; then
                          sleep 10
                      else
                          ### Check if \$ready_status is empty. If NOT, everything is OK.
                          if [[ -z \$ready_status ]]; then echo "Didn't find eny pods"; exit 1; fi
                          break
                      fi
                  ### Check if it's last element from sequnce. If YES, exit from script due to timeout.
                  if [[ \$i == 10 ]]; then echo "TIMEOUT. Check pods status or increase sleep interval"; exit 1; fi
                  done
      """)
}
